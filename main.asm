BITS 64;

%include "lib.inc"
%include "dict.inc"
%include "words.inc"

global _start

%define MAX_BUFFER_SIZE 255
%define EIGHT_BYTES_OFFSET 8

section .bss
	buffer: resb MAX_BUFFER_SIZE

section .rodata
	length_error: db "String must be no longer than 255 bytes", 0
	not_found_error: db "String is not found in dictionary", 0

	%define ERROR_CODE 1


section .text

_start:
	xor rax, rax
	xor rdi, rdi
	mov rsi, buffer
	mov rdx, MAX_BUFFER_SIZE
	push rsi
	syscall 
	pop rdi

	cmp rax, MAX_BUFFER_SIZE
	je .length_error

	mov rsi, search
	call find_word
	test rax, rax
	jz .not_found_error

	add rax, EIGHT_BYTES_OFFSET
	mov rdi, rax
	call string_length

	inc rax
	add rdi, rax
	call print_string

	xor rdi, rdi
	call exit

.length_error:
	mov rdi, length_error
	jmp .print_err

.not_found_error:
	mov rdi, not_found_error

.print_err:
	call print_error
	mov rdi, ERROR_CODE
	call exit
	
