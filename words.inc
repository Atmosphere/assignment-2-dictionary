%include "colon.inc"

section .rodata
; 1
colon "first", first_word
db "first word", 0 
; 2
colon "second", second_word
db "second word", 0
; 3
colon "third", third_word
db "third word", 0 
; 4
colon "four numbers", ottff
db "2024", 0 
; 5
colon "search", search
db "searched word", 0 
