NASM=nasm
NASM_FLAGS=-g -felf64
LD=ld
PYTHON3=python3

lib.o: lib.asm
	$(NASM) $(NASM_FLAGS) $< -o $@ 

dict.o: dict.asm
	$(NASM) $(NASM_FLAGS) $< -o $@

main.o: main.asm words.inc colon.inc
	$(NASM) $(NASM_FLAGS) $< -o $@

main: lib.o dict.o main.o
	$(LD) $< -o $@ 

test: test.py
	$(PYTHON3) $<
