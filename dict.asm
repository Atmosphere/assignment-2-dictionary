BITS 64;

extern string_equals
global find_word

%define OFFSET 8


section .text 

find_word:
	push rsi
	push rdi
.loop:
	mov rdx, rsi
	test rdx, rdx
	jz .not_found

	add rsi, OFFSET
	call string_equals
	cmp rax, 1
	je .found

	mov rsi, [rsi-8]
	jmp .loop

.not_found:
	xor rax, rax
	jmp .ret

.found:
	mov rax, rdx
.ret:
	pop rdi
	pop rsi
	ret

