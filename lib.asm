BITS 64;  

global exit 
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error


	%define SYS_READ 0
	%define FD_STDIN 0
	%define SYS_WRITE 1
	%define FD_STDOUT 1
	%define SYS_EXIT 60
	%define FD_STDERR 2
	
	%define NEWLINE_CHAR 0xA
	%define SPACE_CHAR 0x20
	%define TAB_CHAR 0x9
	%define NULL_TERMINATE_CHAR 0
	%define TEN 0xA

	%define ZERO '0'
	%define NINE '9'
	%define PLUS '+'
	%define MINUS '-'

	%define RESERVE_32_BYTES

section .text
; Принимает код возврата и завершает текущий процесс
exit: 
	mov rax, SYS_EXIT
	syscall  


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax

.loop:
	cmp  byte[rdi + rax], 0
	je .end
	inc rax
	jmp .loop

.end:
	ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	xor rax, rax

	push rdi
	call string_length
	pop rsi
	
	mov rdx, rax
	mov rax, SYS_WRITE
	mov rdi, FD_STDOUT
	syscall

	ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, NEWLINE_CHAR


; Принимает код символа и выводит его в stdout
print_char:
	push rdi 

	mov rax, SYS_WRITE
	mov rdi, FD_STDOUT
	mov rsi, rsp 
	mov rdx, 1
	syscall

	pop rdi
	ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	test rdi, rdi
	jns .printing			;if not 1***...****

	push rdi
	mov rdi, MINUS
	call print_char
	pop rdi
	neg rdi
.printing:


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	push rbx
	mov rbx, rsp
	mov rax, rdi
	mov r8, TEN
	sub rsp, RESERVE_32_BYTES
	dec rbx
	mov byte[rbx], 0

.loop:
	xor rdx, rdx
	div r8
	add rdx, ZERO
	dec rbx
	mov byte[rbx], dl
	test rax, rax
	jne .loop

.printing:
	mov rdi, rbx
	pop rbx
	call print_string
	add rsp, 32
	ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	xor rcx, rcx
.is_equal:
	mov al, byte[rdi + rcx]
	cmp al, byte[rsi + rcx]
	jne .false
	cmp al, 0
	je .true
	inc rcx
	jmp .is_equal
	
.true:
	mov rax, 1
	ret 
.false:
	xor rax, rax
	ret 


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	xor rax, rax
	xor rdi, rdi
	push 0
	mov rsi, rsp
	mov rdx, 1			;number of bytes to read
	syscall

	pop rax
	ret
	

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
	push r12
	push r13
	xor rcx, rcx
	mov r12, rdi
	dec rsi
	mov r13, rsi
.skip_spaces:
	push rcx
	call read_char
	pop rcx
	cmp rax, SPACE_CHAR
	je .skip_spaces
	cmp rax, TAB_CHAR
	je .skip_spaces
	cmp rax, NEWLINE_CHAR
	je .skip_spaces

.reading:
	test rax, rax 
	je .add_0_and_ret
	cmp rax, SPACE_CHAR
	je .add_0_and_ret
	cmp rax, TAB_CHAR
	je .add_0_and_ret
	cmp rax, NEWLINE_CHAR
	je .add_0_and_ret

	cmp r13, rcx 
	je .stop_with_0
	mov byte[r12 + rcx], al 
	inc rcx 
	push rcx
	call read_char
	pop rcx
	jmp .reading

.add_0_and_ret:
	mov byte[r12 + rcx], 0
	mov rax, r12
	mov rdx, rcx
	jmp .ret
.stop_with_0:
	xor rax, rax
.ret:
	pop r13
	pop r12
	ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rax, rax
	xor rdx, rdx
	xor r9, r9
	mov r8, 10

.parsing:
	mov r9b, byte[rdi + rdx]

	cmp r9b, ZERO
	jnge .ret
	cmp r9b, NINE
	jg .ret

	push rdx
	mul r8
	pop rdx
	sub r9, ZERO
	add rax, r9
	inc rdx
	jmp .parsing

.ret:
	ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	xor rax, rax
	xor rdx, rdx
	cmp byte[rdi], PLUS
	je .pos
	cmp byte[rdi], MINUS
	je .neg
	jmp parse_uint

.pos:
	inc rdi
	call parse_uint
	inc rdx
	ret
	
.neg:
	inc rdi
	call parse_uint
	inc rdx
	neg rax
	ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	;rdi -- указатель на строку, rsi -- указатель на буфер , rdx -- длина буфера
	push rdi
	push rsi
	push rdx
	call string_length
	pop rdx
	pop rsi
	pop rdi
	cmp rax, rdx
	jg .stop_with_0

	xor rax, rax
	xor rcx, rcx
	xor r8, r8

.copy:
	mov r8b, byte[rdi + rcx]
	mov byte[rsi + rcx], r8b
	test r8b, r8b
	je .copied
	inc rcx
	jmp .copy

.stop_with_0:
	xor rax, rax
	ret

.copied:
	mov rax, rcx
	ret

print_error:
	  push rdi
    call string_length
    pop  rsi

    mov  rdx, rax 
    mov  rax, SYS_WRITE
    mov  rdi, FD_STDERR
		
    syscall
    ret

