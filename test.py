import subprocess

input = ["first", "second", "search", "fourth", (239+239)*"w"]
output = ["first word", "second word", "searched word", "", ""]
errors = ["", "", "", "String is not found in dictionary", "String must be no longer than 255 bytes"]

for i in range(len(input)):
    proc = subprocess.Popen(["./main"], 
                            stdin=subprocess.PIPE, 
                            stdout=subprocess.PIPE, 
                            stderr=subprocess.PIPE)
    stdout, stderr = proc.communicate(input=input[i].encode())
    stdout = stdout.decode().strip()
    stderr = stderr.decode().strip()
    print("TEST #" + str(i+1) + ": " + input[i])
    
    if (stdout == output[i]) and (stderr == errors[i]):
        print("Result of test " + str(i+1) + ": PASSED")
    else:
        print("Result of test " + str(i+1) + ": FAILED")
        if stdout != output[i]:
            print("Expected OUTPUT in STDOUT: " + output[i])
            print("Real OUTPUT in STDOUT: " + stdout)
        if stderr != errors[i]:
            print("Expected OUTPUT in STDERR: " + errors[i])
            print("Real OUTPUT in STDERR: " + stderr)
